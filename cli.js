
require('events').EventEmitter.prototype._maxListeners = 100;
const lumbermill = require('@lumbermill/node').setGlobalOpts({
    hideContext: true,
});
const logger = lumbermill('emu2mqtt:cli');
const Emu2 = require('./lib/emu2/Emu2');
const MqttClient = require('./lib/MqttClient');
const Controller = require('./lib/Controller');


const emu2 = new Emu2();
const mqtt = new MqttClient();


const controller = new Controller(emu2, mqtt);
try {
    controller.start();
} catch (err) {
    logger.error('unknown error occurred', err);
    setTimeout(() => {
        process.exit(1);
    }, 2000);
    process.exit(1);
}
