const mqtt = require('mqtt');
const lumbermill = require('@lumbermill/node');
const cfg = require('./cfg'); 
const logger = lumbermill('emu2mqtt:mqttclient');

class MqttClient {
    client = null;


    async connect() {
        logger.info(`connecting to mqtt broker at ${cfg.mqttUrl}`);
        this.client = mqtt.connect(cfg.mqttUrl, cfg.mqttCfg);

        this.client.on('reconnect', () => {
            logger.info(`attempting to reconnect to mqtt broker at ${cfg.mqttUrl}`);
            logger.debug(`mqtt options`, cfg.mqttCfg);
        });

        return new Promise((resolve) => {
            this.client.on('connect', function () {
                // if (err) {
                //     logger.error('error connecting to mqtt broker', err);
                //     // reject();
                //     return;
                // }
                logger.info(`connected to mqtt broker at ${cfg.mqttUrl}`);
                resolve();
            });

            // this.client.on('error', () => {
            //     logger.error('error event connecting to mqtt broker');
            // })

        })
    }

    publish(topic, message, options = {}) {
        const payload = typeof message === 'string'
            ? message
            : JSON.stringify(message);
        logger.debug(`publishing [${topic}]: ${payload}`);

        this.client.publish(topic, payload, options);
    }
}

module.exports= MqttClient;