const config = require('config');
const lumbermill = require('@lumbermill/node');

class Config {
    homeAssistant = null;
    mqttUrl = null;
    mqttCfg = {};
    baseTopic = null;
    emuDevice = null;
    homeAssistantDiscovery = true;
    loglevel = 'info';


    constructor({ mqtt, emu, ...other }) {
        this.mqttUrl = mqtt.url;
        this.mqttCfg = mqtt.clientOptions;
        this.discoveryTopic = mqtt.discoveryTopic;
        this.baseTopic = mqtt.baseTopic;
        this.emuDevice = emu.device;
        if (other && Object.keys(other).length > 0) {
            for (const key of Object.keys(other)) {
                this[key] = other[key];
            }
        }

        // configure will
        this.mqttCfg.will = {
            topic: `${this.getBridgeTopic()}/availability`,
            payload: 'offline',
        }
    }

    getBridgeTopic() {
        return `${this.baseTopic}/bridge`;
    }

    getDiscoveryTopicFor(component, nodeId, objectId) {
        return `${this.discoveryTopic}/${component}/${nodeId}/${objectId}/config`;
    }

    getEntityTopicFor(meterMacId, sensorName) {
        return `${this.baseTopic}/${meterMacId}/${sensorName}`;
    }

}

let cfg = new Config(config);
if (cfg.loglevel) {
    lumbermill.setGlobalLogLevel(cfg.loglevel);
    process.env.DEBUG = "emu2mqtt:*";
    lumbermill.refreshPrefixFilters();
}
module.exports = cfg;