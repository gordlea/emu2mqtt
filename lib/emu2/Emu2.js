const SerialPort = require('serialport');
const EventEmitter = require('events');
const lumbermill = require('@lumbermill/node');
const { XMLValidator} = require("fast-xml-parser");
const Regex = require('@serialport/parser-regex')
const cfg = require('../cfg'); 
const parse = require('./parse');
const commands = require('./commands');

const logger = lumbermill('emu2mqtt:emu2', {
    prefixContext: {
        device: cfg.emuDevice,
    }
});

const messageTypes = ['Warning', 'ScheduleInfo', 'NetworkInfo','ApsTable','Information', 'TimeCluster','NwkTable','Information','PriceCluster','DeviceInfo','Google','SimpleMeteringCluster','InstantaneousDemand','BlockPriceDetail','ConnectionStatus','BillingPeriodList','MessageCluster','FastPollStatus','CurrentSummationDelivered','NetworkInfo'];

// const xmlParser = new XMLParser();
// const xmlBuilder = new XMLBuilder();

const typeRegexp = /<(?<type>[A-Za-z]+)>/;

class Emu2 extends EventEmitter {
    enabledListeners = ['DeviceInfo', 'Warning'];
    gotDeviceInfo = false;

    log(level, ...otherArgs) {
        this.logWithContext.apply(this, [level, {}, ...otherArgs]);
    }

    logWithContext(level, context = {}, ...otherArgs) {
        logger.withContext({
            enabledListeners: this.enabledListeners,
            ...context,
        })[level].apply(logger, otherArgs);
    }

    connect() {
        this.log('debug', 'connecting to emu2 device over serial connection')
        const device = cfg.emuDevice;

        return new Promise((resolve, reject) => {
            this.serialPort = new SerialPort(device, {
                baudRate: 115200,
                // dataBits: 8,
                // stopBits: 1,
                // parity: 'none',
            }, (error) => {
                if (error) {
                    this.log('error', 'error connecting to emu device');
                    reject(error);
                }
                this.log('info', 'serial port connection established');
                resolve();
            });
            this.serialPort.on('open', async () => {
                // this.emit('open');
                this.log('info', 'connected to emu2 via serial port');
                // await this.initialize();
                this.listen();
                // setTimeout(() => {
                //     this.log('info', 'getting emu device info');
                //     this.getDeviceInfo();
                // }, 5000);

                const intervalId = setInterval(() => {
                    // keep trying if we don't get it right away
                    if (!this.gotDeviceInfo) {
                        this.log('info', 'getting emu device info');
                        this.getDeviceInfo();
                    }
                }, 5000);

                this.once('DeviceInfo', () => {
                    this.gotDeviceInfo = true;
                    clearInterval(intervalId);
                });
            });
        });
    }

    enableAllListeners() {
        this.enabledListeners = messageTypes;
    }

    setEnabledListeners(listeners = []) {
        this.enabledListeners = listeners;
    }


    listen() {
        // const parser = this.serialPort.pipe(new Readline({ delimiter: '\r\n' }));
        const parser = this.serialPort.pipe(new Regex({ regex: /[\r\n]+(?=<[A-Za-z])/ }));
        // <DeviceMacId>(?<deviceMacId>0x[0-9a-f]+)<\/DeviceMacId>
        parser.on('data', (data) => {
            this.log('debug', 'received data from emu2', data && typeof data === 'string' ? data.split('\n')[0] : data);
            try {
                if (XMLValidator.validate(data, { allowBooleanAttributes: true }) !== true) {
                    this.log("trace", 'xml data was malformed, ignoring\n', data);
                    // console.log(data);
                    return; // ignore malformed xml (sometimes half an xml block comes in at startup)
                }
            } catch {
                // really bad xml, no op
            }
            // console.log(`\n\n${data}`);
            const type = typeRegexp.exec(data)?.groups?.type;


            // if ()
            
            // console.log(type);

            if (!this.enabledListeners.includes(type)) {
                this.log('trace', `got message of type ${type}, but that parser is not enabled`);
                return;
            }
            this.logWithContext('debug', {data}, `got message of type ${type}, parser is enabled`);
            const parseFnc = parse[type] || parse._default;
            // this.log('debug', `found parser function for ${type}, will parse and process`);

            const parsed = parseFnc(data);

            if (type === 'Warning') {
            this.logWithContext('warn', {
                    warning: parsed,
                }, 'got warning from emu-2');
            }
            // console.log(parsed);
            this.emit(type, parsed);
   
        });
    }

    async getDeviceInfo() {
        logger.info('send command: getDeviceInfo');
        return new Promise((resolve, reject) => {
            this.serialPort.write(commands.getDeviceInfo(), (error) => {
                if (error) {
                    console.error('write error:', error);
                    reject(error);
                }
                resolve();
            });
        });
    }

    async getNetworkInfo() {
        logger.info('send command: getNetworkInfo');
        return new Promise((resolve, reject) => {
            this.serialPort.write(commands.getNetworkInfo(), (error) => {
                if (error) {
                    console.error('write error:', error);
                    reject(error);
                }
                resolve();
            });
        });
    }

    async getSchedule(meterMacId, type) {
        logger.info('getSchedule')
        return new Promise((resolve, reject) => {
            this.serialPort.write(commands.getSchedule(meterMacId, type), (error) => {
                if (error) {
                    console.error('write error:', error);
                    reject(error);
                }
                resolve();
            });
        });
    }

    async getInstantaneousDemand(meterMacId, refresh) {
        logger.info('getInstantaneousDemand')
        return new Promise((resolve, reject) => {
            this.serialPort.write(commands.getInstantaneousDemand(meterMacId, refresh), (error) => {
                if (error) {
                    console.error('write error:', error);
                    reject(error);
                }
                resolve();
            });
        });
    }

    // async initialize() {
    //     logger.info('send command: initialize');
    //     return new Promise((resolve, reject) => {
    //         this.serialPort.write(commands.initialize(), (error) => {
    //             if (error) {
    //                 console.error('write error:', error);
    //                 reject(error);
    //             }
    //             resolve();
    //         });
    //     });
    // }

    async setSchedule(meterMacId, type, frequency) {
        return new Promise((resolve, reject) => {
            this.serialPort.write(commands.setSchedule(meterMacId, type, frequency), (error) => {
                if (error) {
                    console.error('write error:', error);
                    reject(error);
                }
                resolve();
            });
        });
    }


    async getCurrentSummation(meterMacId, refresh) {
        logger.info('getCurrentSummation')
        return new Promise((resolve, reject) => {
            this.serialPort.write(commands.getCurrentSummation(meterMacId, refresh), (error) => {
                if (error) {
                    console.error('write error:', error);
                    reject(error);
                }
                resolve();
            });
        });
    }

    async getCurrentPrice(meterMacId, refresh) {
        logger.info('getCurrentPrice')
        return new Promise((resolve, reject) => {
            this.serialPort.write(commands.getCurrentPrice(meterMacId, refresh), (error) => {
                if (error) {
                    console.error('write error:', error);
                    reject(error);
                }
                resolve();
            });
        });
    }

    async getPriceBlocks(meterMacId, refresh) {
        logger.info('getPriceBlocks')
        return new Promise((resolve, reject) => {
            this.serialPort.write(commands.getPriceBlocks(meterMacId, refresh), (error) => {
                if (error) {
                    console.error('write error:', error);
                    reject(error);
                }
                resolve();
            });
        });
    }

    async getMeterAttributes(meterMacId, refresh) {
        logger.info('getMeterAttributes')
        return new Promise((resolve, reject) => {
            this.serialPort.write(commands.getMeterAttributes(meterMacId, refresh), (error) => {
                if (error) {
                    console.error('write error:', error);
                    reject(error);
                }
                resolve();
            });
        });
    }    
}


module.exports = Emu2;
