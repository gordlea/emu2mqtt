const camelCase = require('lodash.camelcase');
const currencies = require('iso4217');

function parseHexString(value) {
    if (typeof value === 'string' || value instanceof String) {
        if (value.indexOf('0x') === 0) {
            return parseInt(value, 16);
        }
    }
    return value;
}

function parseTimestamp(rawValue) {
    const value = parseHexString(rawValue);
    return (value + 946684800) * 1000;
}

function parseMultiplierOrDivisor(rawValue) {
    const value = parseHexString(rawValue);
    if (value === 0) {
        return 1;
    }
    return value;
}

function parseCurrency(rawValue) {
    const value = parseHexString(rawValue);
    const currencyInfo = currencies[value];
    return {
        type: currencyInfo.Code,
        symbol: currencyInfo.Symbol,
    };

}

function parseYNBoolean(rawValue) {
    return rawValue === 'Y';
}



const fieldParsers = {
    timeStamp: parseTimestamp,
    currentStart: parseTimestamp,
    start1: parseTimestamp,
    startTime: parseTimestamp,
    duration: parseHexString,
    demand: parseHexString,
    summationDelivered: parseHexString,
    summationReceived: parseHexString,
    price: parseHexString,
    currency: parseCurrency,
    tier: parseHexString,
    linkStrength: parseHexString,
    frequency: parseHexString,
    enabled: parseYNBoolean,
    blockPeriodConsumption: parseHexString,
    blockPeriodConsumptionDivisor: parseHexString,
    blockPeriodConsumptionMultiplier: parseHexString,
    currentDuration: parseHexString,
    numberOfBlocks: parseHexString,
    currentStart: parseTimestamp,

    trailingDigits: parseHexString,
    multiplier: parseMultiplierOrDivisor,
    divisor: parseMultiplierOrDivisor,
    
}


const metaKeys = [
    'multiplier',
    'divisor',
    'digitsLeft',
    'digitsRight',
    'suppressLeadingZero',
    'trailingDigits',
    'blockPeriodConsumptionDivisor',
    'blockPeriodConsumptionMultiplier',
    'numberOfBlocks',
];

function toJsObject(data, ) {
    let result = {};
    const meta = {};

    const lines = data.trim().split('\n');

    for (const line of lines.slice(1, lines.length - 1)) {
        const tagEndIdx = line.indexOf('>');
        const key = camelCase(line.substring(line.indexOf('<') + 1, tagEndIdx));
        const rawValue = line.substring(tagEndIdx + 1, line.indexOf('</'));

        const value = typeof fieldParsers[key] === 'function'
            ? fieldParsers[key](rawValue)
            : rawValue;

        if (metaKeys.indexOf(key) !== -1) {
            meta[key] = value;
            continue;
        }

        // if (Object.keys(value).length > 0) {
        //     result = {
        //         ...result,
        //         ...value,
        //     };
        // } else {
            result[key] = value;
        // }
    }

    // for (const transformValueKey of transformValues) {
    //     if (meta.multiplier !== undefined) {
    //         result[transformValueKey] = result[transformValueKey] * meta.multiplier;
    //     }
    //     if (meta.divisor !== undefined) {
    //         result[transformValueKey] = result[transformValueKey] / meta.divisor;
    //     }
    //     if (meta.trailingDigits) {
    //         result[transformValueKey] = result[transformValueKey] / Math.pow(10,meta.trailingDigits);
    //     }
    // }
   
    return { meta, result }
}

function doMultiplyDivide(value, multiplier = 1, divisor = 1) {
    const m = multiplier === 0 ? 1 : multiplier;
    const d = divisor === 0 ? 1 : divisor;
    let result = value * m;
    return result / d;
}

function doTrailingDigits(value, trailingDigits) {
    return trailingDigits !== 0
        ? value / Math.pow(10, trailingDigits)
        : value;
}

module.exports = {
    InstantaneousDemand(data) {

        const { meta, result } = toJsObject(data);
        result.demand = doMultiplyDivide(result.demand, meta.multiplier, meta.divisor);
        return result;
    },
    CurrentSummationDelivered(data) {
        const { meta, result } = toJsObject(data);
        result.summationDelivered = doMultiplyDivide(result.summationDelivered, meta.multiplier, meta.divisor);
        result.summationReceived = doMultiplyDivide(result.summationReceived, meta.multiplier, meta.divisor);
        return result;
    },
    PriceCluster(data) {
        const { meta, result } = toJsObject(data);
        result.price = doTrailingDigits(result.price, meta.trailingDigits);
        return result;
    },
    BlockPriceDetail(data) {
        const { meta, result } = toJsObject(data);
        result.blockPeriodConsumption = doMultiplyDivide(result.blockPeriodConsumption, meta.blockPeriodConsumptionMultiplier, meta.blockPeriodConsumptionDivisor)

        const blocks = []
        for (let i = 1; i <= meta.numberOfBlocks; i += 1) {
            let price = parseHexString(result[`price${i}`]);
            price = doTrailingDigits(price, meta.trailingDigits);

            const block = {
                // tier: i - 1,
                price,
            };

            if (i + 1 <= meta.numberOfBlocks) {
                let threshold = parseHexString(result[`threshold${i}`]);
                threshold = doMultiplyDivide(threshold, meta.multiplier, meta.divisor);

                block.threshold = threshold;
                
            } else {
                block.threshold = Infinity;
            }
            blocks.push(block);
        }
        const finalResult = {
            deviceMacId: result.deviceMacId,
            meterMacId: result.meterMacId,
            timeStamp: result.timeStamp,
            currentStart: result.currentStart,
            currentDuration: result.currentDuration,
            blockPeriodConsumption: result.blockPeriodConsumption,
            currency: result.currency,
            priceTiers: blocks,
        };
        // for (const key of Object.keys(result)) {
        //     if (/price\d+/.test(key) || /threshold\d+/.test(key)) {
        //         // don't include these props, we have them in the blocks array now
        //         continue;
        //     }
        //     finalResult[key] = result[key];
        // }

        return finalResult;
    },
    _default(data) {
        const { meta, result } = toJsObject(data);
        return result;
    }
}