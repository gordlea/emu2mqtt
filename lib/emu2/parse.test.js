const parse = require('./parse');

test('parse InstantaneousDemand', () => {
  const result = parse['InstantaneousDemand'](`
  <InstantaneousDemand>
    <DeviceMacId>0xd8d5b900000051ec</DeviceMacId>
    <MeterMacId>0x00078100006f5249</MeterMacId>
    <TimeStamp>0x29329661</TimeStamp>
    <Demand>0x000238</Demand>
    <Multiplier>0x00000001</Multiplier>
    <Divisor>0x000003e8</Divisor>
    <DigitsRight>0x03</DigitsRight>
    <DigitsLeft>0x06</DigitsLeft>
    <SuppressLeadingZero>Y</SuppressLeadingZero>
  </InstantaneousDemand>
`);

  expect(result).toMatchInlineSnapshot(`
Object {
  "demand": 0.568,
  "deviceMacId": "0xd8d5b900000051ec",
  "meterMacId": "0x00078100006f5249",
  "timeStamp": 1637865953000,
}
`);
});

test('parse CurrentSummationDelivered', () => {
  const result = parse['CurrentSummationDelivered'](`
  <CurrentSummationDelivered>
  <DeviceMacId>0xd8d5b900000051ec</DeviceMacId>
  <MeterMacId>0x00078100006f5249</MeterMacId>
  <TimeStamp>0x2932a8d0</TimeStamp>
  <SummationDelivered>0x00000000074cfc9e</SummationDelivered>
  <SummationReceived>0x0000000000000000</SummationReceived>
  <Multiplier>0x00000001</Multiplier>
  <Divisor>0x000003e8</Divisor>
  <DigitsRight>0x01</DigitsRight>
  <DigitsLeft>0x06</DigitsLeft>
  <SuppressLeadingZero>Y</SuppressLeadingZero>
</CurrentSummationDelivered>
`);

  expect(result).toMatchInlineSnapshot(`
Object {
  "deviceMacId": "0xd8d5b900000051ec",
  "meterMacId": "0x00078100006f5249",
  "summationDelivered": 122485.918,
  "summationReceived": 0,
  "timeStamp": 1637870672000,
}
`);
});

test('parse PriceCluster', () => {
  const result = parse['PriceCluster'](`
  <PriceCluster>
    <DeviceMacId>0xd8d5b900000051ec</DeviceMacId>
    <MeterMacId>0x00078100006f5249</MeterMacId>
    <TimeStamp>0x2932a35a</TimeStamp>
    <Price>0x00000580</Price>
    <Currency>0x007c</Currency>
    <TrailingDigits>0x04</TrailingDigits>
    <Tier>0x01</Tier>
    <StartTime>0x2932a8fb</StartTime>
    <Duration>0xffff</Duration>
    <RateLabel>Block 2</RateLabel>
  </PriceCluster>
`);

  expect(result).toMatchInlineSnapshot(`
Object {
  "currency": Object {
    "symbol": "$",
    "type": "CAD",
  },
  "deviceMacId": "0xd8d5b900000051ec",
  "duration": 65535,
  "meterMacId": "0x00078100006f5249",
  "price": 0.1408,
  "rateLabel": "Block 2",
  "startTime": 1637870715000,
  "tier": 1,
  "timeStamp": 1637869274000,
}
`);
});