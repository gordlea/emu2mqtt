const dedent = require('dedent');

function decimalToHex(dec) {
    return `0x${dec.toString()}`;
}

const commands = {
    initialize: () => (dedent`
    <Command>
        <Name>initialize</Name>
    </Command>    
    `),
    getDeviceInfo: () => (dedent`
    <Command>
        <Name>get_device_info</Name>
    </Command>    
    `),
    getNetworkInfo: () => (dedent`
    <Command>
        <Name>get_network_info</Name>
    </Command>
    `),
    getSchedule: (meterMacId = null, type = null) => {
        const optionals = [];
        if (meterMacId !== null) {
            optionals.push(`    <MeterMacId>${meterMacId}</MeterMacId>`);
        }
        if (type !== null) {
            optionals.push(`    <Event>${type}</Event>`);
        }
        const cmd = [
            '<Command>',
            '    <Name>get_schedule</Name>',
            ...optionals,
            '</Command>',
        ].join('\n');
        console.log(cmd);
        return cmd;
    },
    setSchedule: (meterMacId = null, type = null, frequency) => {
        const optionals = [];
        if (meterMacId !== null) {
            optionals.push(`    <MeterMacId>${meterMacId}</MeterMacId>`);
        }


        
        const cmd = [
            '<Command>',
            '    <Name>set_schedule</Name>',
            ...optionals,
            `    <Event>${type}</Event>`,
            `    <Frequency>${decimalToHex(frequency)}</Frequency>`,
            '</Command>',
        ].join('\n');
        console.log(cmd);
        return cmd;
    },    
    getInstantaneousDemand: (meterMacId = null, refresh = null) => {
        const optionals = [];
        if (meterMacId !== null) {
            optionals.push(`    <MeterMacId>${meterMacId}</MeterMacId>`);
        }
        if (refresh !== null) {
            optionals.push(`    <Refresh>${refresh === true ? 'Y' : 'N'}</Refresh>`);
        }
        const command = [
            '<Command>',
            '    <Name>get_instantaneous_demand</Name>',
            ...optionals,
            '</Command>',
        ].join('\n');
        return command;
    },
    getCurrentSummation: (meterMacId = null, refresh = null) => {
        const optionals = [];
        if (meterMacId !== null) {
            optionals.push(`    <MeterMacId>${meterMacId}</MeterMacId>`);
        }
        if (refresh !== null) {
            optionals.push(`    <Refresh>${refresh === true ? 'Y' : 'N'}</Refresh>`);
        }
        const command = [
            '<Command>',
            '    <Name>get_current_summation_delivered</Name>',
            ...optionals,
            '</Command>',
        ].join('\n');
        return command;
    },
    getCurrentPrice: (meterMacId = null, refresh = null) => {
        const optionals = [];
        if (meterMacId !== null) {
            optionals.push(`    <MeterMacId>${meterMacId}</MeterMacId>`);
        }
        if (refresh !== null) {
            optionals.push(`    <Refresh>${refresh === true ? 'Y' : 'N'}</Refresh>`);
        }
        const command = [
            '<Command>',
            '    <Name>get_current_price</Name>',
            ...optionals,
            '</Command>',
        ].join('\n');
        return command;
    },
    getPriceBlocks: (meterMacId = null, refresh = null) => {
        const optionals = [];
        if (meterMacId !== null) {
            optionals.push(`    <MeterMacId>${meterMacId}</MeterMacId>`);
        }
        if (refresh !== null) {
            optionals.push(`    <Refresh>${refresh === true ? 'Y' : 'N'}</Refresh>`);
        }
        const command = [
            '<Command>',
            '    <Name>get_price_blocks</Name>',
            ...optionals,
            '</Command>',
        ].join('\n');
        return command;
    },
    getMeterAttributes: (meterMacId = null, refresh = null) => {
        const optionals = [];
        if (meterMacId !== null) {
            optionals.push(`    <MeterMacId>${meterMacId}</MeterMacId>`);
        }
        if (refresh !== null) {
            optionals.push(`    <Refresh>${refresh === true ? 'Y' : 'N'}</Refresh>`);
        }
        const command = [
            '<Command>',
            '    <Name>get_meter_attributes</Name>',
            ...optionals,
            '</Command>',
        ].join('\n');
        return command;
    }
}

module.exports = commands;