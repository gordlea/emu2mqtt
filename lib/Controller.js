const lumbermill = require('@lumbermill/node');
const cfg = require('./cfg');
const logger = lumbermill('emu2mqtt:controller');

function fixMacAddr(value) {
    const newStr = [];
    for (let i = 2; i < value.length; i += 2) {
        newStr.push(`${value[i]}${value[i + 1]}`);
    }
    return newStr.join(':');
}

class Controller {
    emu2 = null;
    mqtt = null;

    registry = new Map();

    constructor(emu2, mqtt) {
        this.emu2 = emu2;
        this.mqtt = mqtt;
    }

    async start() {
        await this.mqtt.connect();
        await this.emu2.connect();

        this.emu2.on('DeviceInfo', this.handleDeviceInfo);
        this.emu2.on('NetworkInfo', this.handleNetworkInfo);
        this.emu2.on('ScheduleInfo', this.handleScheduleInfo);
        this.emu2.on('InstantaneousDemand', this.handleDemand);
        this.emu2.on('CurrentSummationDelivered', this.handleSummationDelivered);
        this.emu2.on('PriceCluster', this.handlePriceCluster);
        this.emu2.on('BlockPriceDetail', this.handleBlockPrices);
        this.emu2.on('CustomMeterAttributes', this.handleMeterAttributes);
    }

    handleDeviceInfo = (deviceInfo) => {
        const { deviceMacId, fwVersion, manufacturer, modelId } = deviceInfo;

        const registryId = `device_${deviceMacId}`;
        const firstRun = !this.registry.has(registryId);

        this.registry.set(registryId, {
            identifiers: [
                deviceMacId
            ],
            manufacturer,
            connections: [
                ['mac', fixMacAddr(deviceMacId)],
            ],
            model: modelId,
            name: 'EMU-2',
            sw_version: fwVersion,
        });

        if (firstRun) {
            this.mqtt.publish(`${cfg.getBridgeTopic()}/availability`, 'online', {
                retain: true
            });
            this.emu2.setEnabledListeners(['NetworkInfo']);
        }
        this.emu2.getNetworkInfo();
    }

    handleNetworkInfo = (networkInfo) => {
        const { status, deviceMacId } = networkInfo;

        const connectivity = status === 'Connected' ? 'ON' : 'OFF';
        const stateTopic = `${cfg.getBridgeTopic()}/state`;
        const attributesTopic = `${cfg.getBridgeTopic()}/attributes`;

        this.mqtt.publish(stateTopic, connectivity, {
            retain: true
        });
        this.mqtt.publish(attributesTopic, {
            ...networkInfo,
        }, {
            retain: true
        });

        const registryId = `network_${deviceMacId}`;
        const uniqueId = `${deviceMacId}_connectivity`;
        const firstRun = !this.registry.has(registryId);
        if (firstRun) {
            if (cfg.homeAssistantDiscovery === true) {
                const discoveryPayload = {
                    name: 'EMU-2 Connected to Meter',
                    device_class: 'connectivity',
                    state_topic: stateTopic,
                    // value_template: "{{ value_json.state }}",
                    unique_id: uniqueId,
                    json_attributes_topic: attributesTopic,
                    // json_attributes_template: "{{ value_json.attributes }}",
                    device: this.registry.get(`device_${deviceMacId}`),
                    availability: [
                        {
                            topic: `${cfg.getBridgeTopic()}/availability`,
                        }
                    ]
                }
                this.mqtt.publish(cfg.getDiscoveryTopicFor('binary_sensor', 'emu2', uniqueId), discoveryPayload, {
                    retain: true,
                })
            }
            this.emu2.enableAllListeners();
            this.emu2.getInstantaneousDemand();
            this.registry.set(registryId, true);
        }
    }

    beginPollSummation = (meterMacId) => {
        this.emu2.getCurrentPrice(meterMacId, true);
        this.emu2.getCurrentSummation(meterMacId, true);
        setInterval(() => {
            this.emu2.getCurrentSummation(meterMacId, true);
            this.emu2.getCurrentPrice(meterMacId, true);
            this.emu2.getNetworkInfo();
        }, 60000);
    }

    handleDemand = (instantDemand) => {
        const name = 'EMU-2 Meter Power Demand';
        const { deviceMacId, meterMacId, demand } = instantDemand;

        const registryId = `demand_${deviceMacId}_${meterMacId}`;
        const uniqueId = `${deviceMacId}_${meterMacId}_demand`;
        const firstRun = !this.registry.has(registryId);

        const stateTopic = `${cfg.getEntityTopicFor(meterMacId, name)}/state`;
        this.mqtt.publish(stateTopic, demand, {
            retain: true
        });


        const device = {
            name: 'Electricity Meter',
            identifiers: [
                meterMacId,
            ],
            manufacturer: cfg.meter.manufacturer,
            model: cfg.meter.model,
            connections: [
                ['mac', fixMacAddr(meterMacId)],
            ],
            via_device: deviceMacId,
        }

        if (firstRun && cfg.homeAssistantDiscovery) {
            const discoveryPayload = {
                name: name,
                device_class: 'power',
                unit_of_measurement: 'kW',
                state_topic: stateTopic,
                unique_id: uniqueId,
                state_class: 'measurement',
                device,
                availability: [
                    {
                        topic: `${cfg.getBridgeTopic()}/availability`,
                    }
                ]
            }
            this.mqtt.publish(cfg.getDiscoveryTopicFor('sensor', 'emu2', uniqueId), discoveryPayload, {
                retain: true,
            })
            this.registry.set(registryId, true);
            this.beginPollSummation();
            this.emu2.getPriceBlocks();
        }

    }

    handleSummationDelivered = (delivered) => {
        const name = 'EMU-2 Meter Energy Consumption';
        const nameReceived = 'EMU-2 Meter Energy Returned to Grid';
        const { deviceMacId, meterMacId, summationDelivered, summationReceived } = delivered;

        const registryId = `sd_${deviceMacId}_${meterMacId}`;
        const uniqueId = `${deviceMacId}_${meterMacId}_from_grid`;
        const uniqueIdReceived = `${deviceMacId}_${meterMacId}_to_grid`;
        const firstRun = !this.registry.has(registryId);

        const stateTopic = `${cfg.getEntityTopicFor(meterMacId, name)}/state`;
        this.mqtt.publish(stateTopic, summationDelivered, {
            retain: true
        });
        const stateTopicReceived = `${cfg.getEntityTopicFor(meterMacId, nameReceived)}/state`;

        this.mqtt.publish(stateTopicReceived, summationReceived, {
            retain: true
        });


        const device = {
            identifiers: [
                meterMacId,
            ],
            manufacturer: cfg.meter.manufacturer,
            model: cfg.meter.model,
            connections: [
                ['mac', fixMacAddr(meterMacId)],
            ],
            via_device: deviceMacId,
        }

        if (firstRun && cfg.homeAssistantDiscovery) {
            const discoveryPayload = {
                name: name,
                device_class: 'energy',
                unit_of_measurement: 'kWh',
                state_topic: stateTopic,
                unique_id: uniqueId,
                state_class: 'total_increasing',
                device,
                availability: [
                    {
                        topic: `${cfg.getBridgeTopic()}/availability`,
                    }
                ]
            }
            this.mqtt.publish(cfg.getDiscoveryTopicFor('sensor', 'emu2', uniqueId), discoveryPayload, {
                retain: true,
            });
            const discoveryReceivedPayload = {
                name: nameReceived,
                device_class: 'energy',
                unit_of_measurement: 'kWh',
                state_topic: stateTopicReceived,
                unique_id: uniqueIdReceived,
                state_class: 'total_increasing',
                device,
                availability: [
                    {
                        topic: `${cfg.getBridgeTopic()}/availability`,
                    }
                ]
            }
            this.mqtt.publish(cfg.getDiscoveryTopicFor('sensor', 'emu2', uniqueIdReceived), discoveryReceivedPayload, {
                retain: true,
            })
            this.registry.set(registryId, true);
        }
    }

    handlePriceCluster = (priceCluster) => {
        logger.log('handlePriceCluster', priceCluster);

        const currentPriceName = 'EMU-2 Meter Current Price';
        const priceTierName = 'EMU-2 Meter Current Price Tier';
        const { deviceMacId, meterMacId, currency, price, tier } = priceCluster;

        const registryId = `pricecluster_${deviceMacId}_${meterMacId}`;
        const uniqueIdPrice = `${deviceMacId}_${meterMacId}_current_price`;
        const uniqueIdTier = `${deviceMacId}_${meterMacId}_current_price_tier`;

        const firstRun = !this.registry.has(registryId);

        const stateTopicPrice = `${cfg.getEntityTopicFor(meterMacId, currentPriceName)}/state`;
        this.mqtt.publish(stateTopicPrice, price, {
            retain: true
        });

        const stateTopicPriceTier = `${cfg.getEntityTopicFor(meterMacId, priceTierName)}/state`;

        this.mqtt.publish(stateTopicPriceTier, tier, {
            retain: true
        });

        const tierAttributesTopic = `${cfg.getEntityTopicFor(meterMacId, priceTierName)}/attributes`;



        const device = {
            identifiers: [
                meterMacId,
            ],
            manufacturer: cfg.meter.manufacturer,
            model: cfg.meter.model,
            connections: [
                ['mac', fixMacAddr(meterMacId)],
            ],
            via_device: deviceMacId,
        };


        if (firstRun && cfg.homeAssistantDiscovery) {
            // this.emu2.setSchedule(meterMacId, 'summation', 20);

            // this.emu2.getSchedule(meterMacId);
            const discoveryPayloadPrice = {
                name: currentPriceName,
                device_class: 'monetary',
                unit_of_measurement: `${currency.type}/kWh`,
                state_topic: stateTopicPrice,
                unique_id: uniqueIdPrice,
                state_class: 'measurement',
                device,
                availability: [
                    {
                        topic: `${cfg.getBridgeTopic()}/availability`,
                    }
                ]
            }
            this.mqtt.publish(cfg.getDiscoveryTopicFor('sensor', 'emu2', uniqueIdPrice), discoveryPayloadPrice, {
                retain: true,
            })

            const discoveryPayloadTier = {
                name: priceTierName,
                state_topic: stateTopicPriceTier,
                unique_id: uniqueIdTier,
                state_class: 'measurement',
                device,
                json_attributes_topic: tierAttributesTopic,
                availability: [
                    {
                        topic: `${cfg.getBridgeTopic()}/availability`,
                    }
                ]
            }
            this.mqtt.publish(cfg.getDiscoveryTopicFor('sensor', 'emu2', uniqueIdTier), discoveryPayloadTier, {
                retain: true,
            })

            this.registry.set(registryId, true);
        }
    }

    handleMeterAttributes = (priceCluster) => {
        logger.log('handleMeterAttributes', priceCluster);
    }

    handleBlockPrices = (blockPrices) => {
        logger.log('handleBlockPrices', blockPrices);
        const { meterMacId } = blockPrices;
        const priceTierName = 'EMU-2 Meter Price Tier';
        const tierAttributesTopic = `${cfg.getEntityTopicFor(meterMacId, priceTierName)}/attributes`;
        this.mqtt.publish(tierAttributesTopic, {
            ...blockPrices,
        }, {
            retain: true
        });
    }

    handleScheduleInfo = (scheduleInfo) => {
        logger.log('scheduleInfo', scheduleInfo);
    }
}

module.exports = Controller;