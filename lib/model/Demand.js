
class Demand {
    constructor(props) {
        // console.log('Demand.constructor', props);
        this.type = 'demand';
        this.id = props.id;
        this.timestamp = props.timestamp;
        this.demand = props.demand;
        this.deviceMacId = props.deviceMacId;
        this.meterMacId = props.meterMacId;
    }
}
module.exports = Demand;
