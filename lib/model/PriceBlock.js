
class PriceBlock {
    constructor(props) {
        this.id = props.id;
        this.timestamp = props.timestamp;
        this.start = props.start;
        this.deviceMacId = props.deviceMacId;
        this.meterMacId = props.meterMacId;
    }
}

module.exports = PriceBlock;
