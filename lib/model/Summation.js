
class Summation {
    constructor(props) {
        this.id = props.id;
        this.type = 'summation';
        this.timestamp = props.timestamp;
        this.delivered = props.delivered;
        this.received = props.received;
        this.deviceMacId = props.deviceMacId;
        this.meterMacId = props.meterMacId;
    }
}

module.exports = Summation;
