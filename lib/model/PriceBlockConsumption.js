
class PriceBlockConsumption {
    constructor(props) {
        this.id = props.id;
        this.timestamp = props.timestamp;
        this.priceBlock = props.priceBlock;
        this.consumption = props.consumption;
    }
}

module.exports = PriceBlockConsumption;
