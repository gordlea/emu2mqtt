
class PriceBlock {
    constructor(props) {
        this.id = props.id;
        this.timestamp = props.timestamp;
        this.blockTier = props.blockTier;
        this.price = props.price;
        this.threshold = props.threshold;
    }
}

module.exports = PriceBlock;
