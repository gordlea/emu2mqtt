#!/usr/bin/env bashio

EMU2MQTT_MQTT_URL=$(bashio::config 'mqttUrl')
export EMU2MQTT_MQTT_URL
EMU2MQTT_MQTT_USERNAME=$(bashio::config 'mqttUsername')
export EMU2MQTT_MQTT_USERNAME
EMU2MQTT_MQTT_PASSWORD=$(bashio::config 'mqttPassword')
export EMU2MQTT_MQTT_PASSWORD
# EMU2MQTT_MQTT_PROTOCOL_VERSION=$(bashio::config 'mqttProtocolVersion')
# export EMU2MQTT_MQTT_PROTOCOL_VERSION
EMU2MQTT_DEVICE=$(bashio::config 'emuDevice')
export EMU2MQTT_DEVICE
EMU2MQTT_LOGLEVEL=$(bashio::config 'loglevel')
export EMU2MQTT_LOGLEVEL
EMU2MQTT_METER_MANUFACTURER=$(bashio::config 'meterManufacturer')
export EMU2MQTT_METER_MANUFACTURER
EMU2MQTT_METER_MODEL=$(bashio::config 'meterModel')
export EMU2MQTT_METER_MODEL

export DEBUG="emu2mqtt:*"

yarn node --trace-warnings ./cli.js